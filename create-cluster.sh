#!/bin/bash
KEY_PAIR=appointments-cluster
    ecs-cli up \
      --keypair $KEY_PAIR  \
      --capability-iam \
      --size 1 \
      --instance-type t3.medium \
      --tags project=appointments-cluster,owner=matias \
      --cluster-config appointments \
      --ecs-profile appointments  

